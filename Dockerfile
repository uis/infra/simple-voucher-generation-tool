FROM python:3.8

# set the working directory in the container
WORKDIR /workdir

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

COPY . /workdir

# command to run on container start
CMD [ "python", "./generate-vouchers.py" ]

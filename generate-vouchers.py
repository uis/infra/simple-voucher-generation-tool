import configparser
import csv
import json
import time
import uuid

import jwt

config = configparser.ConfigParser()
config.read('config.ini')


def create_voucher_header(env):
    """
    Returns voucher's header.

    :param env: voucher's environment.
    :return: iss, aud, voucher header
    """

    (iss, aud) = (config[env]['iss'], config[env]['aud'])

    header = {}
    header['alg'] = config['DEFAULT']['alg']
    header['typ'] = config['DEFAULT']['typ']

    header['iss'] = iss
    header['aud'] = aud

    return iss, aud, header


def get_private_key(env):
    """
    Returns the private key to be used to create the vouchers.

    """
    with open(config[env]['private_key'], 'rb') as key_file:
        return key_file.read()


def generate_vouchers(vouchers_spec, env):
    """
    Generates vouchers according to the spec file passed in argument.

    Format of vouchers_spec file:
    `School or College | Group IDs | Group names | AD domain | CRSID | Size (TB) | \
        Duration (Yrs) | Value (GBP) | Invalid before | Expiry date | Account | Comment`
    Example:
    `School; ENG; Department of Engineering; blue.cam.ac.uk; abc12; 4; 1; 600; \
        17/04/2020 00:00:00; 17/04/2021 00:00:00; TBD; Test`

    """
    iss, aud, header = create_voucher_header(env)

    private_key = get_private_key(env)

    vouchers_dict = {}
    vouchers_list = []

    with open(vouchers_spec) as csvfile:
        read_csv = csv.reader(csvfile, delimiter=';')
        for row in read_csv:
            # remove trailing and leading spaces in columns
            row = [col.strip() for col in row]

            # iat (issued at) claim is set to have the same value as nbf claim (not before)
            iat = int(
                time.mktime(time.strptime(row[8], config['DEFAULT']['date_format'])))
            nbf = int(
                time.mktime(time.strptime(row[8], config['DEFAULT']['date_format'])))
            exp = int(
                time.mktime(time.strptime(row[9], config['DEFAULT']['date_format'])))

            payload = {}
            payload['jti'] = uuid.uuid4().hex
            payload['iss'] = iss
            payload['aud'] = aud
            payload['crsid'] = row[4]
            payload['len'] = row[6]
            payload['val'] = row[7]
            payload['iat'] = iat
            payload['nbf'] = nbf
            payload['exp'] = exp
            payload['auto_renewal'] = row[12]
            # https://github.com/jpadilla/pyjwt/issues/505
            token = jwt.encode(
                payload, private_key, algorithm='ES256', headers=header
                )

            voucher = {}
            voucher['School or College'] = row[0]
            voucher['Group IDs'] = row[1]
            voucher['Group names'] = row[2]
            voucher['AD domain'] = row[3]
            voucher['CRSID'] = row[4]
            voucher['Size (TB)'] = row[5]
            voucher['Duration (Yrs)'] = row[6]
            voucher['Value (GBP)'] = row[7]
            voucher['Invalid before'] = row[8]
            voucher['Expiry date'] = row[9]
            voucher['Voucher ID'] = payload['jti']
            voucher['Voucher'] = token
            voucher['Account'] = row[10]
            voucher['Comment'] = row[11]
            voucher['Auto-renewal'] = row[12]

            vouchers_dict[payload['jti']] = voucher

            vouchers_list.append(
                [voucher['School or College'], voucher['Group IDs'], voucher['Group names'],
                 voucher['AD domain'], voucher['CRSID'], voucher['Size (TB)'], voucher['Duration (Yrs)'],
                 voucher['Value (GBP)'], voucher['Invalid before'], voucher['Expiry date'], voucher['Voucher ID'],
                 voucher['Voucher'], voucher['Account'], voucher['Comment'], voucher['Auto-renewal']]
            )

    vouchers_json_file = f"{config['DEFAULT']['data_dir']}/vouchers.json"
    vouchers_csv_file = f"{config['DEFAULT']['data_dir']}/vouchers.csv"

    with open(vouchers_json_file, 'w') as outfile_json:
        print('Writing vouchers in %s' % vouchers_json_file)
        json.dump(vouchers_dict, outfile_json, indent=4)

    with open(vouchers_csv_file, 'w') as outfile_csv:
        print('Writing vouchers in %s' % vouchers_csv_file)
        writer = csv.writer(outfile_csv, delimiter=';')
        writer.writerows(vouchers_list)

if __name__ == "__main__":
    generate_vouchers(
        config['DEFAULT']['vouchers_spec_file'], config['DEFAULT']['env'])

## Simple voucher generation tool

The Python script generates vouchers based on a specification file in CSV format located by default in `./data/` directory.

The test and production variables are defined in [`config.ini`](https://gitlab.developers.cam.ac.uk/uis/infra/simple-voucher-generation-tool/-/blob/master/config.ini).

If you want to use the dockerised version of the script, go to [Use Docker container](https://gitlab.developers.cam.ac.uk/uis/infra/simple-voucher-generation-tool#use-docker-container).

### Installation

Clone the repository:
```bash
$ git clone git@gitlab.developers.cam.ac.uk:uis/infra/simple-voucher-generation-tool.git
$ cd simple-voucher-generation-tool
```
To use `virtualenv`:
```bash
$ virtualenv -p python3.7 venv
$ . venv/bin/activate
```
Install the requirements:
```bash
$ pip install -r requirements.txt
```

### Generate the keys
Generate the [Elliptic Curve](https://tools.ietf.org/html/rfc7518#section-3.4) (EC) public and private keys, skip this step if you already have keys.

Production environment keys:
```bash
$ cd simple-voucher-generation-tool/keys/
$ openssl ecparam -genkey -name prime256v1 -noout -out ec_private_production.pem
$ openssl ec -in ec_private_production.pem -pubout -out ec_public_production.pem
```
Test environment keys:
```bash
$ cd simple-voucher-generation-tool/keys/
$ openssl ecparam -genkey -name prime256v1 -noout -out ec_private_test.pem
$ openssl ec -in ec_private_test.pem -pubout -out ec_public_test.pem
```

Make sure the names of `.pem` files correspond to the configuration in [`config.ini`](https://gitlab.developers.cam.ac.uk/uis/infra/simple-voucher-generation-tool/-/blob/master/config.ini).

### Run the script
Given the following specification:
```bash
$ cat ./data/vouchers_spec.csv
# "School" or "College" | Group IDs | Group names | AD domain | CRSID | Size (TB) | Duration (Yrs) | Value (GBP) | Invalid before | Expiry date | Account | Comment | Auto-renewal ("True" or "False")
School; ABC; Department ABC; ad.domain; abc12; 4; 1; 600;17/04/2020 00:00:00;17/04/2021 00:00:00; TBD; Test; True
```

To generate the vouchers:
```bash
$ python generate-vouchers.py 
Writing vouchers in ./data/vouchers.json
Writing vouchers in ./data/vouchers.csv
```

The generated vouchers are stored by default in `./data/vouchers.json`.
```json
{
    "021b4c45e40347c5bdef84d0eb1a0395": {
        "School or College": "School",
        "Group IDs": "ABC",
        "Group names": "Department ABC",
        "AD domain": "ad.domain",
        "CRSID": "abc12",
        "Size (TB)": "4",
        "Duration (Yrs)": "1",
        "Value (GBP)": "600",
        "Invalid before": "17/04/2020 00:00:00",
        "Expiry date": "17/04/2021 00:00:00",
        "Voucher ID": "021b4c45e40347c5bdef84d0eb1a0395",
        "Voucher": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImlzcyI6Imlmcy1saXZlLWVpYmFoN2hhaDgiLCJhdWQiOiJzc2d3In0.eyJqdGkiOiIwMjFiNGM0NWU0MDM0N2M1YmRlZjg0ZDBlYjFhMDM5NSIsImlzcyI6Imlmcy1saXZlLWVpYmFoN2hhaDgiLCJhdWQiOiJzc2d3IiwidmFsIjoiNjAwIiwiY3JzaWQiOiJhYmMxMiIsImlhdCI6MTU4NzA3ODAwMCwibmJmIjoxNTg3MDc4MDAwLCJleHAiOjE2MTg2MTQwMDB9.xbYv5OzjfYvmB5in8TVZITanLIP4ni923n4kMNiHIutzzJagBp758lhckMGECskN8U2fT4ElEjTVW9yrw0bsXA",
        "Account": "TBD",
        "Comment": "Test",
        "Auto-renewal": "true"
    }
}
```
You can verify the voucher (`Voucher` key in the json object above) on the [SSGW Voucher Debugger](https://rjw57.github.io/voucher-tool/).

### Use Docker container
Before running the container, you might want to create voucher spec file and the encryption keys, see above for details.
```bash
$ tree
.
├── my_config.ini
├── my_data
│   └── vouchers_spec.csv
└── my_keys
    ├── ec_private_production.pem
    ├── ec_private_test.pem
    ├── ec_public_production.pem
    └── ec_public_test.pem

2 directories, 6 files

$ docker run -it --rm \
> -v $PWD/my_config.ini:/workdir/config.ini \
> -v $PWD/my_keys:/workdir/keys \
> -v $PWD/my_data:/workdir/data \
> registry.gitlab.developers.cam.ac.uk/uis/infra/simple-voucher-generation-tool
Writing vouchers in ./data/vouchers.json
Writing vouchers in ./data/vouchers.csv
```